<?php
session_start();


	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "uds";

	$conn = new mysqli($servername, $username, $password, $database);

	if ($conn->connect_error) {
  	die("Connection failed: " . $conn->connect_error);
}


$srno = $_GET['srno'];
$editdata = mysqli_query($conn,"select * from sign_up where srno = '$srno'");
$data = mysqli_fetch_array($editdata);
$status = $data['status'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/style.css">
	<title>Edit User</title>

</head>
<body>
	<div>
        <form action="update_data.php" method="post">
            <label for="email">Srno</label><br>
            <input type="hidden" name="srno" value="<?php echo $data['srno']; ?>" required><br>
            <label for="email">Email</label><br>
            <input type="email" name="email" value="<?php echo $data['email']; ?>" required><br>
            <label for="name">Name</label><br>
            <input type="text" name="name" value="<?php echo $data['name']; ?>" required><br>
            <label for="address">Address</label><br>
            <input type="text" name="address" value="<?php echo $data['address']; ?>"><br>
            <label for="password">Password</label><br>
            <input type="text" name="pass" value="<?php echo $data['password']; ?>"required><br>
            <label for="status">Status</label><br>
            <select name="status">
                <option value="pending"<?php if($status=='panding'){ echo "selected";}?>>pending</option>
                <option value="accept"<?php if($status=='accept'){ echo "selected";}?>>accept</option>
                <option value="reject"<?php if($status=='reject'){ echo "selected";}?>>reject</option>
            </select><br>
            <button type="submit" name="submit">Update</button><br><br>
        </form>
	</div>
</body>
</html>