<?php
session_start();
if(isset($_SESSION['email'])){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "uds";
	$conn = new mysqli($servername, $username, $password, $database);
	if ($conn->connect_error) {
  	die("Connection failed: " . $conn->connect_error);
	}
	$user_details=mysqli_query($conn,"select * from sign_up where status='accept'");
	$nums = mysqli_num_rows($user_details);
?>
	

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/style.css">

	<title>User Listing Page</title>
</head>
<body>
	<nav>
		<a href="user_listing.php" title="Home Page">Home</a>
		<a href="logout.php" title="Logout">Logout</a>
	</nav>
	<div>
		<h1>Approved User List</h1>
		<table>
			<thead>
				<tr>
					<th>User name</th>
					<th>Email</th>
					<th>Address</th>
				</tr>
			</thead>
			<tbody>
				<?php
				while($res=mysqli_fetch_array($user_details)){
				?>
				<tr >
					<td><?php echo $res['name'];?></td>
					<td><?php echo $res['email'];?></td>
					<td><?php echo $res['address']?></td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>

	
</body>
</html>

<?php
}
else{
	echo "<p id='login-fail'>Please Login First<p>";
}

?>
